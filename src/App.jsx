import { useEffect, useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import Header from './components/Header'
import Products from './components/Products'

function App() {
const [products, setProducts] = useState([])

useEffect(()=>{
  fetch('https://fakestoreapi.com/products/')
            .then(res=>res.json())
            .then(json=>setProducts(json))
},[])
  return (
    <>
      <Header/>
      <main className='container mx-auto p-6 '>
        <section>
          <h1 className='text-lg font-bold'>New Styles</h1>
          <ul className='p-4 grid md:grid-cols-2 lg:grid-cols-3 gap-4 xl:grid-cols-4'>
            {
              products.map(product=>{
                return(
                    <Products key={product.id} product={product}/>
                )
              })
            }
          </ul>
        </section>
      </main>
      <footer>

      </footer>
    </>
  )
}

export default App
