import React from "react";
import { useSelector } from "react-redux";


function Header(props){
    const cartItems = useSelector(state=> state.cart.cartItems)
    return(
        <header className='h-24 flex flex-row justify-between items-center p-4 shadow-lg bg-cyan-300	 '>
      <span>Urbanic</span>
        <nav className='hidden md:block'>
          <ul className='flex flex-row gap-2'>
            <li>
              <a href='#'>Home</a>
            </li>
            <li>
              <a href='#'>Contact</a>
            </li>
            <li>
              <a href='#'>About</a>
            </li>
            <li>
              <a href='#'>Profile</a>
            </li>
          </ul>
        </nav>
        <button className='md:hidden'>
          <img className='w-6 h-6' src="/icons/mainbar.svg" alt="" />
        </button>
        <a className='hidden md:block' href='#'>
          <div className='flex flex-row'>
            <img className='w-6 h-6 ' src="/icons/carticon.svg" alt="" />
            <span className='text-xs text-red-800'>{cartItems.length}</span>
          </div>
        </a>
      </header>
    )
}

export default Header;