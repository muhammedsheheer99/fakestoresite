import React from "react";
import { useDispatch } from "react-redux";
import { addToCart } from "../features/cart/cartSlice";

function Products(props){
    const dispatch = useDispatch()
    const product = props.product
    return(
        <li className="flex flex-col justify-between gap-8">
          <div>
              <img className='w-full aspect-square object-contain object-center	 ' src={product.image}></img>
              <h3>{product.title}</h3>
              <span>{product.price}$</span>
          </div>
          <button onClick={()=>{dispatch(addToCart({product: product, quantity:1}))}} className="w-full bg-red-400 h-8">Add to cart</button>
        </li>
    )
}

export default Products;